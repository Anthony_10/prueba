import { Component, OnInit } from '@angular/core';
import { ApiService } from '../ApiService';
import { Producto } from '../interfaces';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {
  lista: Producto[] = [];
  producto?: Producto = undefined;
  indice: number = -1;
  mensaje: string = "";
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.api.obternerProductos().subscribe( data => {
      this.lista = data.lista;
    });
  }
  verProducto(): void{
    this.producto = this.lista[this.indice];
    this.mensaje = "Producto elegido";
  }
}
