import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent} from './inicio/inicio.component'
import { ListadoComponent } from './listado/listado.component';
import { LoginComponent } from './login/login.component';
import { ProductoComponent } from './producto/producto.component';
import { RegistroComponent } from './registro/registro.component';

const routes: Routes = [
  {path:"",           component: InicioComponent},
  {path:"productos",  component: ListadoComponent},
  {path:"registro",  component: RegistroComponent},
  {path:"producto/:codigo",  component: ProductoComponent},
  {path:"login",  component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
