import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ApiService } from '../ApiService';
import { SesionUsuario, Usuario } from '../interfaces';
import * as action from './../action';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  correo: string = '';
  clave: string = '';
  mensaje: string = '';
  constructor(
    private api: ApiService,
    private storeSesion: Store<{sesionUsuario: any}>,
    private route: Router) { }

  ngOnInit(): void {
  }
  ingresar(): void{
    const usuario: Usuario = {
      id: 0,
      nombres: '',
      apellidos: '',
      correo: this.correo,
      clave: this.clave
    }
    this.api.obtenerUsuario(usuario).subscribe( respuesta => {
      console.log("respuesta id: " + respuesta.id);
      if(respuesta.id != 0){
        this.mensaje = 'Validación exitosa';
        const sesionUsuario: SesionUsuario ={
          id: respuesta.id,
          correo: respuesta.correo,
          nombres: respuesta.nombres + ' ' + respuesta.apellidos
        };  
        this.storeSesion.dispatch(action.sesionUsuario(sesionUsuario));
        this.route.navigateByUrl("productos");
      }
      else{
        this.mensaje = 'Los datos ingresados son erróneos';
      }
    });
  }
}
