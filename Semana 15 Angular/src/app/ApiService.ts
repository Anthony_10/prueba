import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { RespuestaProducto, Usuario } from "./interfaces";
import { retry, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json;charset=utf-8'
        })
    };
    errorHandl(error: any) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } 
        else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }    
        console.log(errorMessage);
        return throwError(errorMessage);
    }
    constructor(private http: HttpClient) { }  
    obtenerProductos(): Observable<RespuestaProducto> {
        return this.http.post<RespuestaProducto>('http://127.0.0.1:8081/obtener-productos', null, this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
    }

    agregarUsuario(data:Usuario): Observable<Usuario> {
        return this.http.post<Usuario>('http://127.0.0.1:8081/agregar-usuario', data, this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
    }

    obtenerUsuario(data:Usuario): Observable<Usuario> {
        return this.http.post<Usuario>('http://127.0.0.1:8081/autenticar-usuario', data, this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
    }
}
