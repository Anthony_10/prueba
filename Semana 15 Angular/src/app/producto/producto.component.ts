import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../ApiService';
import { Producto } from '../interfaces';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit {
  producto?: Producto = undefined;
  lista: Producto[] = [];
  indice:number = 0;
  constructor(private api: ApiService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.indice = Number(this.activatedRoute.snapshot.paramMap.get('codigo'));
    this.api.obtenerProductos().subscribe( data => {
      this.lista = data.lista;
      this.producto = this.lista[this.indice];
    });
  }
}
