import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ApiService } from '../ApiService';
import { Producto, SesionUsuario } from '../interfaces';
import { initialSesionUsuario } from '../reducer';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {
  lista: Producto[] = [];
  producto?: Producto = undefined;
  indice: number = -1;
  mensaje: string = "";
  /*{
    id: undefined,
    marca: undefined,
    nombre: undefined,
    peso: undefined,
    precioRegular: undefined,
    precioOferta: undefined,
    oferta: undefined,
    calificacion: undefined,
    urlImagen: undefined
  };*/

  sesionUsuario$ : Observable<any>;  
  sesionUsuario : SesionUsuario;

  constructor(private api: ApiService,private storeSesion: Store<{sesionUsuario: any}>) { }

  ngOnInit(): void {
    this.sesionUsuario = initialSesionUsuario;    
    this.sesionUsuario$ = this.storeSesion.pipe(select('sesionUsuario'));    
    this.sesionUsuario$.subscribe( reaccion => {      
      this.sesionUsuario = reaccion;
    });

    this.api.obtenerProductos().subscribe( data => {
      this.lista = data.lista;
    });
  }
  

}
