import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './inicio/inicio.component';
import { ListadoComponent } from './listado/listado.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { ProductoComponent } from './producto/producto.component';
import { StoreModule } from '@ngrx/store';
import { sesionUsuarioReducer } from './reducer';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    ListadoComponent,
    LoginComponent,
    RegistroComponent,
    ProductoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot({sesionUsuario: sesionUsuarioReducer})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
