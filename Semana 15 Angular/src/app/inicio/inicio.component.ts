import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  mensaje: string = '';
  elementos: string[] = [];
  cantidad: number = 0;
  constructor() { }

  ngOnInit(): void {
  }
  cambiarMensaje(): void{
    this.mensaje = 'Ya es tarde';
  }
  iterarElementos():void{
    for (let indice = 0; indice < this.cantidad; indice++) {
      this.elementos[indice] = 'Mensaje' + indice;
    }
  }

}
