export interface Producto {
	idProducto?: number;
    marca?:string;
    nombre?: string;
    precioUnitario?: number;
    imagen?: string;
}

export interface RespuestaProducto {
	productos: Producto[];
}

export interface Usuario{
    id?: number;
	email?:string;
	nombres?:string;
	apellidos?:string;
	perfil?: number;
}

export interface UsuarioLogin{
    email?:string;
	contraseña?:string;
}

export interface UsuarioNuevo{
    email?:string;
	contraseña?:string;
	nombres?:string;
	apellidos?:string;
}