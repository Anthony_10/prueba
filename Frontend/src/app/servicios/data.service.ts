import { Injectable } from '@angular/core';
import { Producto } from 'Frontend-Proyecto/src/app/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  //lista: Producto[] = []; 
  productos: Producto[] = [];
  constructor() { }
}
