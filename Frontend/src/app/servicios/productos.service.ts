import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RespuestaProducto } from 'Frontend-Proyecto/src/app/interfaces';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json;charset=utf-8'
    })
  };
  errorHandl(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    errorMessage = error.error.message;
    console.log(errorMessage);
    return throwError(errorMessage);
  }
  url: string = `http://localhost:8080/buscar-productos-nombre`;
  constructor(private http: HttpClient) { }

  buscarProductosPorNombre(nombre: string){
    return this.http.get<RespuestaProducto>(`${this.url}/${nombre}`);
    //return this.http.get<RespuestaProducto>('http://localhost:8080/buscar-productos-nombre/{nombreProducto}');
  }
}
