import { Component, OnInit } from '@angular/core';
import { ApiService } from '../ApiService';
import { Producto } from '../interfaces';
import { DataService } from '../servicios/data.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {
  lista: Producto[] = [];
  producto?: Producto = undefined;
  nombre: string = '';
  constructor(private api: ApiService, public dataService: DataService) { }

  ngOnInit(): void {

    console.log("DataService: " + this.dataService)
    /*this.api.obternerProductos().subscribe( data => {
      this.lista = data.productos;
    });*/

  }


}
