# En un colegio de nivel inicial, necesariamente para que se abra un
# aula debe haber hasta 8 alumnos, de exceder esta cantidad se tiene
# que abrir una nueva aula. El colegio solo dispone de 2 aulas “A” y
# “B”, por tanto no se permite más de 16  alumnos  en  el  colegio.  Se
# ingresa  solo  la  fecha  de  nacimiento  de  cada  alumno  (ejemplo:
# 25/07/2020).  Diseñar  un  algoritmo  que  pregunte  la  cantidad  de
# alumnos, para que a cada uno se ingrese solo su fecha de
# nacimiento,  me  reporte  cuantas  aulas  se  a  abierto  y  la  lista  de
# alumnos por aula de manera ordenada ascendentemente por fecha
# de nacimiento.
from datetime import datetime

def validarCantidadAlumnos():
    while True:
        cantidad = int(input("Ingrese la cantidad de alumnos [1, 16]: "))
        if cantidad >= 1 and cantidad <= 16:
            return cantidad
            break

def abrirOtroSalon(cantidad):
    B = []
    print("\n\t.:SALÓN B:.")
    for i in range(cantidad):
        print("Ingrese su fecha de nacimiento (DD/MM/AAAA): ", end="")
        fechaDeNacimiento = input()
        B.append(fechaDeNacimiento)
    return B

def abrirSalon(cantidad):
    A = []
    print("\n\t.:SALÓN A:.")
    for i in range(cantidad):
        print("Ingrese su fecha de nacimiento (DD/MM/AAAA): ", end="")
        fechaDeNacimiento = input()
        A.append(fechaDeNacimiento)
    return A

def printSalonA(dates):
    print("\n\t.:SALÓN A, ORDENADO POR FECHA DE NACIMIENTO:.")
    for i in range(len(dates)):
        print(f"{i + 1}. {dates[i]}")

def printSalonB(dates):
    print("\n\t.:SALÓN B, ORDENADO POR FECHA DE NACIMIENTO:.")
    for i in range(len(dates)):
        print(f"{i + 1}. {dates[i]}")

def salones(cantidad):
    if cantidad == 16:
        cociente = cantidad // 2
        cantidad -= cociente
        lista1 = abrirSalon(cociente)
        lista2 = abrirSalon(cociente)
        lista1.sort(key=lambda date: datetime.strptime(date, '%d/%m/%Y'))
        lista2.sort(key=lambda date: datetime.strptime(date, '%d/%m/%Y'))
        printSalonA(lista1)
        printSalonB(lista2)

    elif cantidad > 8 and cantidad < 16:
        resto = cantidad % 8
        cantidad -= resto
        lista1 = abrirSalon(cantidad)
        lista2 = abrirOtroSalon(resto)
        lista1.sort(key=lambda date: datetime.strptime(date, '%d/%m/%Y'))
        lista2.sort(key=lambda date: datetime.strptime(date, '%d/%m/%Y'))
        printSalonA(lista1)
        printSalonB(lista2)

    else:
        lista1 = abrirSalon(cantidad)
        lista1.sort(key=lambda date: datetime.strptime(date, '%d/%m/%Y'))
        printSalonA(lista1)

def main():
    cantidad = validarCantidadAlumnos()
    salones(cantidad)
main()