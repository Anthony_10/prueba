def validarCategoria(lista):
    while True:
        categoria = input("Ingrese su categoría (A,B o C): ")
        for sublista in lista:
            if sublista[0] == categoria:
                return categoria
                break

def validarPromedio():
    while True:
        promedio = float(input("Ingrese su promedio dentro del rango [0 - 20]: "))
        if promedio >= 0 and promedio <= 20:
            return promedio
            break

def validarLugarDeProcedencia(lista):
    while True:
        lugar = input("Ingrese su lugar de procedencia: ")
        for sublista in lista:
            if sublista[0] == lugar:
                return lugar
                break

def promedioAlumno(promedio, lista, categoria):
    if promedio >= 0 and promedio <= 11:
        for sublista in lista:
            if sublista[0] == categoria:
                return sublista[1]

    elif promedio >= 12 and promedio <= 15:
        for sublista in lista:
            if sublista[0] == categoria:
                rebaja = sublista[1] * 0.10
                sublista[1] -= rebaja
                return sublista[1]

    elif promedio >= 16 and promedio <= 18:
        for sublista in lista:
            if sublista[0] == categoria:
                rebaja = sublista[1] * 0.12
                sublista[1] -= rebaja
                return sublista[1]

    elif promedio >= 19 and promedio <= 20:
        for sublista in lista:
            if sublista[0] == categoria:
                rebaja = sublista[1] * 0.15
                sublista[1] -= rebaja
                return sublista[1]

def lugarProcedencia(procedencia, lista, lugares, categoria):
    for lugar in lugares:
        if lugar[0] == procedencia:
            for sublista in lista:
                if sublista[0] == categoria:
                    rebaja = sublista[1] * lugar[1]
                    sublista[1] -= rebaja
                    return sublista[1]

def rebajaPension(nuevaPension, categoria, lista):
    for sublista in lista:
        if sublista[0] == categoria:
            rebaja = sublista[1] - nuevaPension
            return round(rebaja, 3)

def main():
    categoriaPension = [['A', 550], ['B', 500], ['C', 460]]
    listaAuxiliar = [['A', 550], ['B', 500], ['C', 460]]
    lugares = [["Lima", 0], ["Provincia Costa", 0.1], ["Provincia Sierra", 0.15], ["Provincia Selva", 0.2]]
    categoria = validarCategoria(categoriaPension)
    promedio = validarPromedio()
    lugar = validarLugarDeProcedencia(lugares)
    promedioAlumno(int(promedio), categoriaPension, categoria)
    nuevaPension = lugarProcedencia(lugar, categoriaPension, lugares, categoria)
    print(f"\nLa rebaja que recibirá es: {rebajaPension(nuevaPension, categoria, listaAuxiliar)}")
    print(f"La nueva pensión a pagar es: {nuevaPension}")
main()