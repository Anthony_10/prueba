# Escriba  un  programa  que  pida  10  numero  enteros  de  manera
# aleatoria en un rango de 0 a 100 y me muestre cual es el valor más
# cercano al promedio. De haber varios con la misma cercanía también
# debe mostrarlos.
import random
from heapq import nsmallest

def promedio(lista):
    prom = sum(lista) / len(lista)
    return prom

def resta(promedio, lista):
    restaNumerosPromedio = []
    for numero in lista:
        resta = abs(promedio - numero)
        restaNumerosPromedio.append(resta)
    return restaNumerosPromedio

def obtenerCantidad(lista):
    return lista.count(min(sorted(lista)))

def masCercano(numero, lista):
    return nsmallest(numero, lista, key=lambda x: abs(x - promedio(lista)))

def main():
    numerosAleatorios = [random.randint(0, 100) for i in range(10)]
    print(f"Numeros aleatorios: {numerosAleatorios}")
    print(f"\nPromedio: {promedio(numerosAleatorios)}")
    numero = obtenerCantidad(resta(promedio(numerosAleatorios), numerosAleatorios))
    print("\nLos numero mas cercanos son: ", end="")
    acumulador = ''
    for numero in set(masCercano(numero, numerosAleatorios)):
        acumulador += str(numero) + ", "
    print(acumulador[:-2])

main()