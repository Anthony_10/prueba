# Existe un arreglo de tamaño 5 que representa una cola en el banco
# para  hacer  un  pago  en  ventanilla.  Escriba  un  programa  que  pida
# ingresar un número entero aleatorio como nuevo dato a la cola, así
# también pida eliminar un dato de la cola (que representa que sale de
# la cola para ser atendido en ventanilla).
# Tener en cuenta que al agregar un dato se coloca al final de la cola
# y al eliminar un dato de la cola se elimina el que está adelante y todos
# corren un asiento. Ventanilla está cerca al índice 0 de la cola.
# Mostrar la cola (arreglo) cada vez que se realiza una acción (agregar
# o eliminar)
import random
def agregar(lista):
    print("\nSe ingreso un numero aleatorio a la cola ")
    numero = random.randint(0, 1000)
    lista.append(numero)
    print(f"\nCola actual: {lista}")

def eliminar(lista):
    if len(lista) > 0:
        print(f"\nAtendiendo a: {lista.pop(0)}")
        print(f"\nCola actual: {lista}")
    else:
        print("\nCola vacía, nadie para atender")

def opciones(lista):
    while True:
        print("\n\t.:MENÚ DE OPCIONES:.")
        print("1. Agregar")
        print("2. Eliminar")
        print("3. Salir")
        opcion = int(input("Elija una opción: "))
        if opcion == 1:
            agregar(lista)
        elif opcion == 2:
            eliminar(lista)
        elif opcion == 3:
            print("\nGracias por utilizar el programa :D")
            break
        else:
            print("Esa opción no existe")

def main():
    colaBanco = [random.randint(0, 1000) for i in range(5)]
    print(f"Cola banco: {colaBanco}")
    opciones(colaBanco)
main()